package menko.model;

public class ModelImpl implements Model {
    private BinaryTreeMap<Integer, String> treeMap;

    public ModelImpl() {
        treeMap = new BinaryTreeMap();
        treeMap.put(30, "thirty");
        treeMap.put(26, "twenty six");
        treeMap.put(73, "seventy three");
        treeMap.put(8, "eight");
        treeMap.put(2, "two");
        treeMap.put(54, "fifty four");
        treeMap.put(33, "thirty three");
        treeMap.put(98, "Windows 98");
        treeMap.put(121, "one hundred twenty one");
        treeMap.put(14, "fourteen");
        treeMap.put(72, "seventy two");
        treeMap.put(66, "sixty six");
        treeMap.displayTree();
    }


    @Override
    public void putNewNode(int key, String value) {
        treeMap.put(key, value);
        treeMap.print();
    }

    @Override
    public void removeNode(int key) {
        treeMap.remove(key);
        treeMap.print();
    }

    @Override
    public String getValueByKey(int key) {
        return treeMap.get(key);
    }

    @Override
    public void printTree() {
        treeMap.displayTree();
    }
}
